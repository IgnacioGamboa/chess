using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace Samples.DragDrop
{
	/// <summary>
	/// Interaction logic for DragCanvasExample.xaml
	/// </summary>

	public partial class Board : Window
	{

		public Board()
		{
			InitializeComponent();
            DrawBorders();

        }

        void DrawBorders()
        {
            for (int i = 1; i < 9; i++)
            {
                TextBlock tb = new TextBlock();
                tb.Text = i.ToString();
                tb.FontSize = 35;
                tb.HorizontalAlignment = HorizontalAlignment.Center;
                tb.SetValue(Grid.RowProperty, 0);
                tb.SetValue(Grid.ColumnProperty, i);
                board.Children.Add(tb);
            }

            char[] letters = new char[8] {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H'};
            foreach (var item in letters)
            {
            
                TextBlock tb = new TextBlock();
                tb.Text = item.ToString();
                tb.FontSize = 35;
                tb.VerticalAlignment = VerticalAlignment.Center;
                tb.HorizontalAlignment = HorizontalAlignment.Center;
                // this 64 comes from here: https://www.ascii-code.com/ 
                tb.SetValue(Grid.RowProperty, item-64);
                tb.SetValue(Grid.ColumnProperty, 0);
                board.Children.Add(tb);
            }

            for (int i = 1; i < 10; i++)
            {
                var b1 = new Border();
                b1.BorderThickness = new Thickness(3, 0, 0, 0);
                b1.BorderBrush = new SolidColorBrush(Colors.Green);
                b1.SetValue(Grid.RowProperty, 1);
                b1.SetValue(Grid.ColumnProperty, i);
                b1.SetValue(Grid.RowSpanProperty, 8);

                //board.ShowGridLines = true;
                board.Children.Add(b1);
            
            }

            for (int i = 1; i < 10; i++)
            {
                var b1 = new Border();
                b1.BorderThickness = new Thickness(0, 3, 0, 0);
                b1.BorderBrush = new SolidColorBrush(Colors.Green);
                b1.SetValue(Grid.RowProperty, i);
                b1.SetValue(Grid.ColumnProperty, 1);
                b1.SetValue(Grid.ColumnSpanProperty, 8);

                //board.ShowGridLines = true;
                board.Children.Add(b1);

            }
        }

	}
}